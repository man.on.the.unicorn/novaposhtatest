# Requirements

- docker
- docker-compose

# Start project

1. `docker-compose up`
2. `docker-compose exec app php artisan migrate`
3. Paste NOVA_POSHTA_BASE_URL and NOVA_POSHTA_API_KEY to `nova-poshta-app/.env`

Database should be available at port `33059`. MySQL env variables located in `docker/.env`

> You should manager docker as a non-root user. Otherwise use 'sudo' when executing docker/docker-compose commands

# Nova poshta commands

- `docker-compose exec app php artisan app:nova-poshta:fetch` - fetch list of all cities and branches
- `docker-compose exec app php artisan app:nova-poshta:update-city {cityId} {--no-delete}` - update city && its branches

# How to add new delivery service

- define new type in `Andry\DeliveryLibrary\DeliveryServices\DeliveryType`
- Add implementation for type
- Add DI for implemented service in `Andry\DeliveryServiceProvider\Provider\DeliveryServiceProvider`
- Manage it from `Andry\DeliveryServiceProvider\Manager\DeliveryServiceManager->get(DeliveryType::{YOUT_TYPE})`

# Improve list

- [ ] Split repository in multiple (library / service-provider / laravel application)
- [ ] Remove unused laravel dependencies
- [ ] Make service-provider autoconfiguration for implementation of `DeliveryService's`
- [ ] Interface `DeliveryService` is non-stable. Expand arguments as a DTO's with supporting in each implementation
- [ ] Add `page` / `perPage` as command options
- [ ] Add tests for NovaPoshta `getBranches` API method
- [ ] Migrate psalm to 8.1!
