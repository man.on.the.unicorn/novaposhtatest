<?php

namespace Andry\DeliveryServiceProvider\Manager;

use Andry\DeliveryLibrary\DeliveryServices\DeliveryType;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\ConfigurationSchema;

return [
    DeliveryType::NOVA_POSHTA->value => [
        ConfigurationSchema::BASE_URL => env('NOVA_POSHTA_BASE_URL'),
        ConfigurationSchema::API_KEY => env('NOVA_POSHTA_API_KEY'),
    ],
];
