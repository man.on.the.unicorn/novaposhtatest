<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Init extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nova_poshta_city', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('city_id', 256)->primary();
            $table->string('city_name', 512);
            $table->timestamps();
        });

        Schema::create('nova_poshta_branch', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('branch_id', 256)->primary();
            $table->string('branch_name', 512);
            $table->string('latitude', 512)->nullable();
            $table->string('longitude', 512)->nullable();
            $table->string('city_id');
            $table->timestamps();

            $table
                ->foreign('city_id')
                ->references('city_id')
                ->on('nova_poshta_city')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nova_poshta_city');
        Schema::dropIfExists('nova_poshta_branch');
    }
}
