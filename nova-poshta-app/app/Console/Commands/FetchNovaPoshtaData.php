<?php declare(strict_types=1);

namespace App\Console\Commands;

use Andry\DeliveryLibrary\Contracts\Service\Exception\CannotGetModel;
use App\Service\NovaPoshtaService;
use Illuminate\Console\Command;
use Throwable;

final class FetchNovaPoshtaData extends Command
{
    protected $signature = 'app:nova-poshta:fetch';

    protected $description = 'Update city from NovaPoshta API';

    public function __invoke(NovaPoshtaService $service)
    {
        $page = 1;
        $perPage = 5;

        do {
            try {
                $cities = $service->retrieveCitiesFromApi($page, $perPage);
            } catch (CannotGetModel $exception) {
                $this->error(sprintf("Getting cities from API exception: %s", $exception->getMessage()));
                $cities = [];
            }

            foreach ($cities as $city) {
                try {
                    $this->runCommand(UpdateCityCommand::class, ['cityId' => $city->city_id], $this->output);
                } catch (Throwable $throwable) {
                    $this->error(sprintf("Unexpected exception occurred: %s", $throwable->getMessage()));
                }
            }

            $page++;
        } while (!empty($cities));
    }
}
