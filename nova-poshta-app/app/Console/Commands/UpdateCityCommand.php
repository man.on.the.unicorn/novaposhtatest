<?php declare(strict_types=1);

namespace App\Console\Commands;

use Andry\DeliveryLibrary\Contracts\Service\Exception\CannotGetModel;
use App\Models\NovaPoshtaBranch;
use App\Models\NovaPoshtaCity;
use App\Service\NovaPoshtaService;
use Illuminate\Console\Command;

final class UpdateCityCommand extends Command
{
    private const CITY_ID_ARG = 'cityId';

    protected $signature = 'app:nova-poshta:update-city {cityId} {--no-delete}';

    protected $description = 'Update city from NovaPoshta API';

    public function __invoke(NovaPoshtaService $service): void
    {
        try {
            $city = $this->updateCity($service);
        } catch (CannotGetModel $exception) {
            $this->error(sprintf("Update city, API error: %s", $exception->getMessage()));
            return;
        }

        if (null === $city) {
            return;
        }

        $this->updateBranches($city, $service);
    }

    private function updateBranches(NovaPoshtaCity $city, NovaPoshtaService $service): void
    {
        $page = 1;
        $perPage = 10;
        $updateStart = new \DateTimeImmutable();

        $this->output->info(sprintf("Starting update branches for city %s", $city->city_name));
        $this->getOutput()->progressStart();

        do {
            try {
                $branches = $service->retrieveBranchesForCityFromApi($city, $page, $perPage);
            } catch (CannotGetModel $exception) {
                $this->warn(sprintf("Update branches, API error: %s", $exception->getMessage()));
                continue;
            }

            foreach ($branches as $branch) {
                $branch->saveOrUpdate();
            }

            $this->getOutput()->progressAdvance($perPage);

            $page++;
        } while (!empty($branches));

        $this->getOutput()->progressFinish();
        $this->output->info("Finished update branches, deleting old ones...");

        NovaPoshtaBranch::where('updated_at', '<', $updateStart)->where('city_id', $city->city_id)->delete();
    }

    private function updateCity(NovaPoshtaService $service):? NovaPoshtaCity
    {
        $cityId = $this->argument(self::CITY_ID_ARG);

        if (!is_string($cityId)) {
            $this->error("Not valid cityId. Please, input cityId as string");
            return null;
        }

        $city = $service->retrieveCityFromApi($cityId);

        if (null === $city && !$this->option('--no-delete')) {
            $this->warn("No city in NovaPoshta found. Deleting current...");
            NovaPoshtaCity::destroy($cityId);
        }

        return $city?->saveOrUpdate();
    }
}
