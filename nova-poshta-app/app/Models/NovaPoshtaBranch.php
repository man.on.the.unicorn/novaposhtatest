<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property string branch_id
 * @property string branch_name
 * @property string|null latitude
 * @property string|null longitude
 * @property string city_id
 * @property NovaPoshtaCity city
 */
final class NovaPoshtaBranch extends ExtendedModel
{
    protected $primaryKey = 'branch_id';

    protected $table = 'nova_poshta_branch';

    public $incrementing = false;

    public static function create(
        string $id,
        string $name,
        ?string $latitude,
        ?string $longitude,
        NovaPoshtaCity $city
    ) {
        $branch = new self();
        $branch->branch_id = $id;
        $branch->branch_name = $name;
        $branch->latitude = $latitude;
        $branch->longitude = $longitude;
        $branch->city_id = $city->city_id;
        return $branch;
    }

    public function city(): BelongsTo
    {
        return $this->belongsTo(NovaPoshtaCity::class);
    }
}
