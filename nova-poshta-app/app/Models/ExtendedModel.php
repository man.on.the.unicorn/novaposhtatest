<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class ExtendedModel extends Model
{
    /**
     * @psalm-return $this
     * @return Model
     */
    public function saveOrUpdate(): Model
    {
        if (!$this->exists) {
            /** @var Model|null $existingModel */
            $existingModel = self::find($this->getAttribute($this->primaryKey));

            if (null === $existingModel) {
                $this->touch();
                $this->save();
                return $this;
            }

            foreach ($this->attributes as $attributeKey => $attributeValue) {
                $existingModel->{$attributeKey} = $attributeValue;
            }

            $existingModel->touch();
            $existingModel->save();
            return $existingModel;
        } else {
            $this->touch();
            $this->save();
            return $this;
        }
    }
}
