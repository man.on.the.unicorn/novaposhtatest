<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string city_id
 * @property string city_name
 * @property Collection branches
 */
final class NovaPoshtaCity extends ExtendedModel
{
    protected $primaryKey = 'city_id';

    protected $table = 'nova_poshta_city';

    public $incrementing = false;

    public function branches(): HasMany
    {
        return $this->hasMany(NovaPoshtaBranch::class, 'city_id', 'city_id');
    }

    public static function create(string $id, string $name): self
    {
        $city = new self();
        $city->city_id = $id;
        $city->city_name = $name;
        return $city;
    }
}
