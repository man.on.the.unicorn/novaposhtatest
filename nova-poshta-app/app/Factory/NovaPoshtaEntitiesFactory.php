<?php declare(strict_types=1);

namespace App\Factory;

use Andry\DeliveryLibrary\Contracts\Service\Branch\Branch;
use Andry\DeliveryLibrary\Contracts\Service\City\City;
use App\Models\NovaPoshtaBranch;
use App\Models\NovaPoshtaCity;

final class NovaPoshtaEntitiesFactory
{
    public function cityFromLibrary(City $city): NovaPoshtaCity
    {
        return NovaPoshtaCity::create($city->id, $city->name);
    }

    public function branchFromLibrary(Branch $branch, NovaPoshtaCity $city): NovaPoshtaBranch
    {
        return NovaPoshtaBranch::create($branch->id, $branch->name, $branch->latitude, $branch->longitude, $city);
    }
}
