<?php declare(strict_types=1);

namespace App\Service;

use Andry\DeliveryLibrary\Contracts\Service\Branch\Branch;
use Andry\DeliveryLibrary\Contracts\Service\City\City;
use Andry\DeliveryLibrary\Contracts\Shared\Pagination;
use Andry\DeliveryLibrary\DeliveryServices\DeliveryType;
use Andry\DeliveryServiceProvider\Manager\DeliveryServiceManager;
use App\Factory\NovaPoshtaEntitiesFactory;
use App\Models\NovaPoshtaBranch;
use App\Models\NovaPoshtaCity;

final class NovaPoshtaService
{
    public function __construct(private DeliveryServiceManager $manager, private NovaPoshtaEntitiesFactory $factory)
    {
    }

    /**
     * @param int $page
     * @param int $perPage
     * @return array<NovaPoshtaCity>
     */
    public function retrieveCitiesFromApi(int $page, int $perPage): array
    {
        $cities = $this->manager->get(DeliveryType::NOVA_POSHTA)->searchCity(
            new Pagination($page, $perPage),
        );

        return array_map(
            fn (City $city) => $this->factory->cityFromLibrary($city),
            $cities->items,
        );
    }

    public function retrieveCityFromApi(string $cityId):? NovaPoshtaCity
    {
        $cityFromApi = $this->manager->get(DeliveryType::NOVA_POSHTA)->getCity($cityId);

        if (null === $cityFromApi) {
            return null;
        }

        return $this->factory->cityFromLibrary($cityFromApi);
    }

    /**
     * @param NovaPoshtaCity $city
     * @param int $page
     * @param int $perPage
     * @return array<NovaPoshtaBranch>
     */
    public function retrieveBranchesForCityFromApi(NovaPoshtaCity $city, int $page, int $perPage): array
    {
        $branches = $this->manager->get(DeliveryType::NOVA_POSHTA)->searchBranch(
            pagination: new Pagination($page, $perPage),
            cityId: $city->city_id,
        );

        return array_map(
            fn (Branch $branch) => $this->factory->branchFromLibrary($branch, $city),
            $branches->items,
        );
    }
}
