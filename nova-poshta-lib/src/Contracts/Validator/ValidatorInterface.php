<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\Contracts\Validator;

/**
 * @psalm-template T
 */
interface ValidatorInterface
{
    /**
     * @param T $object
     * @return void
     * @throws ValidationException
     */
    public function validate(object $object): void;
}