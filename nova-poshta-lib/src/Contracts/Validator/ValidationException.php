<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\Contracts\Validator;

final class ValidationException extends \DomainException
{

}