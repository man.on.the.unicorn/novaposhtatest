<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\Contracts\Validator;

final class NullValidator implements ValidatorInterface
{
    /**
     * @inheritDoc
     */
    public function validate(object $object): void
    {
    }
}