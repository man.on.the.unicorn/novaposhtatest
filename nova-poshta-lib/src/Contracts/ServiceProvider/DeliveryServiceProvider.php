<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\Contracts\ServiceProvider;

use Andry\DeliveryLibrary\Contracts\Configuration\ConfigurationBag;
use Andry\DeliveryLibrary\Contracts\Service\DeliveryService;

interface DeliveryServiceProvider
{
    public function provide(ConfigurationBag $configuration): DeliveryService;
}