<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\Contracts\Configuration;

final class ConfigurationBag
{
    /**
     * @param array<string, string> $configuration
     */
    public function __construct(private array $configuration = [])
    {
    }

    public function get(string $key): string
    {
        if (!isset($this->configuration[$key])) {
            throw new ConfigKeyDoesNotExists("No key $key found");
        }

        return $this->configuration[$key];
    }
}