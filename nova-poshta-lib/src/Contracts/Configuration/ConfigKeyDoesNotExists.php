<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\Contracts\Configuration;

final class ConfigKeyDoesNotExists extends \DomainException
{

}