<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\Contracts\Service;

use Andry\DeliveryLibrary\Contracts\Service\Branch\Branch;
use Andry\DeliveryLibrary\Contracts\Service\Branch\BranchSearchParamBag;
use Andry\DeliveryLibrary\Contracts\Service\City\City;
use Andry\DeliveryLibrary\Contracts\Service\Exception\CannotGetModel;
use Andry\DeliveryLibrary\Contracts\Shared\Pagination;
use Andry\DeliveryLibrary\Contracts\Shared\PaginationResult;

/**
 * @psalm-template T
 */
interface DeliveryService
{
    /**
     * @return DeliveryTypeId
     */
    public function identifier(): DeliveryTypeId;

    /**
     * @param string $cityId
     * @return City|null
     * @throws CannotGetModel
     */
    public function getCity(string $cityId):? City;

    /**
     * @param Pagination $pagination
     * @return PaginationResult<City>
     * @throws CannotGetModel
     */
    public function searchCity(Pagination $pagination): PaginationResult;

    /**
     * @param string $branchId
     * @return Branch|null
     * @throws CannotGetModel
     */
    public function getBranch(string $branchId):? Branch;

    /**
     * @param string|null $cityId
     * @param Pagination $pagination
     * @param BranchSearchParamBag|null $bag
     * @return PaginationResult<Branch>
     * @throws CannotGetModel
     */
    public function searchBranch(Pagination $pagination, string $cityId = null, BranchSearchParamBag $bag = null): PaginationResult;
}