<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\Contracts\Service;

use Andry\DeliveryLibrary\Contracts\Service\Branch\Branch;
use Andry\DeliveryLibrary\Contracts\Service\Branch\BranchSearchParamBag;
use Andry\DeliveryLibrary\Contracts\Service\City\City;
use Andry\DeliveryLibrary\Contracts\Service\Exception\CannotGetModel;
use Andry\DeliveryLibrary\Contracts\Shared\Pagination;
use Andry\DeliveryLibrary\Contracts\Shared\PaginationResult;
use Throwable;

final class ExceptionContractDecorator implements DeliveryService
{
    public function __construct(private DeliveryService $service)
    {
    }

    /**
     * @inheritDoc
     */
    public function identifier(): DeliveryTypeId
    {
        return $this->service->identifier();
    }

    /**
     * @inheritDoc
     */
    public function getCity(string $cityId): ?City
    {
        try {
            return $this->service->getCity($cityId);
        } catch (Throwable $throwable) {
            throw new CannotGetModel($throwable->getMessage(), 0, $throwable);
        }
    }

    /**
     * @inheritDoc
     */
    public function searchCity(Pagination $pagination): PaginationResult
    {
        try {
            return $this->service->searchCity($pagination);
        } catch (Throwable $throwable) {
            throw new CannotGetModel($throwable->getMessage(), 0, $throwable);
        }
    }

    /**
     * @inheritDoc
     */
    public function getBranch(string $branchId): ?Branch
    {
        try {
            return $this->service->getBranch($branchId);
        } catch (Throwable $throwable) {
            throw new CannotGetModel($throwable->getMessage(), 0, $throwable);
        }
    }

    /**
     * @inheritDoc
     */
    public function searchBranch(Pagination $pagination, string $cityId = null, BranchSearchParamBag $bag = null): PaginationResult
    {
        try {
            return $this->service->searchBranch($pagination, $cityId, $bag);
        } catch (Throwable $throwable) {
            throw new CannotGetModel($throwable->getMessage(), 0, $throwable);
        }
    }
}