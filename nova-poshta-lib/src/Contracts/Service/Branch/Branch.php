<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\Contracts\Service\Branch;

final class Branch
{
    public function __construct(
        public readonly string $id,
        public readonly string $cityId,
        public readonly string $name,
        public readonly string $latitude,
        public readonly string $longitude,
    ) {
    }
}