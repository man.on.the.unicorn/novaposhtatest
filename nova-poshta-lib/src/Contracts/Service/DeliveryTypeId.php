<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\Contracts\Service;

interface DeliveryTypeId
{
    public function identifierAsString(): string;
}