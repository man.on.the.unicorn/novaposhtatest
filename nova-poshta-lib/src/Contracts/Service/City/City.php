<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\Contracts\Service\City;

final class City
{
    public function __construct(
        public readonly string $id,
        public readonly string $name,
    ) {
    }
}