<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\Contracts\Service\Exception;

final class CannotGetModel extends \DomainException
{

}