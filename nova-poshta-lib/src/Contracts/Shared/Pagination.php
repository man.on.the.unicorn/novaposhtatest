<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\Contracts\Shared;

final class Pagination
{
    public function __construct(
        public readonly int $page,
        public readonly int $perPage,
    ) {
    }
}