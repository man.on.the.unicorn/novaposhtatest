<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\Contracts\Shared;

/**
 * @psalm-template T
 */
final class PaginationResult
{
    /**
     * @param Pagination $pagination
     * @param bool $hasNextPage
     * @param array<T> $items
     */
    public function __construct(
        public readonly Pagination $pagination,
        public readonly bool $hasNextPage,
        public readonly array $items,
    ) {
    }
}