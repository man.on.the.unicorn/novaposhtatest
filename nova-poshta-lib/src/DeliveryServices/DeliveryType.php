<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices;

use Andry\DeliveryLibrary\Contracts\Service\DeliveryTypeId;

enum DeliveryType: string implements DeliveryTypeId
{
    case NOVA_POSHTA = 'nova_poshta';

    public function identifierAsString(): string
    {
        return $this->value;
    }
}