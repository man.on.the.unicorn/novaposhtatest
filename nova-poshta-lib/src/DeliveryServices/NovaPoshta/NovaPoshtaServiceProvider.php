<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta;

use Andry\DeliveryLibrary\Contracts\Configuration\ConfigurationBag;
use Andry\DeliveryLibrary\Contracts\Service\DeliveryService;
use Andry\DeliveryLibrary\Contracts\Service\ExceptionContractDecorator;
use Andry\DeliveryLibrary\Contracts\ServiceProvider\DeliveryServiceProvider;
use Andry\DeliveryLibrary\Contracts\Validator\NullValidator;
use Andry\DeliveryLibrary\Contracts\Validator\ValidatorInterface;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Adapter\RequestAdapter;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\NovaPoshtaHttpClient;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\RequestFactory;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Service\NovaPoshtaService;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

final class NovaPoshtaServiceProvider implements DeliveryServiceProvider
{
    public function __construct(
        private SerializerInterface $serializer,
        private ValidatorInterface $validator = new NullValidator(),
        private ClientInterface $client = new Client(),
        private LoggerInterface $logger = new NullLogger(),
    ) {
    }

    public function provide(ConfigurationBag $configuration): DeliveryService
    {
        return new ExceptionContractDecorator(
            new NovaPoshtaService(
                new NovaPoshtaHttpClient(
                    $configuration->get(ConfigurationSchema::BASE_URL),
                    $this->client,
                    $this->validator,
                    $this->serializer,
                    $this->logger,
                ),
                new RequestFactory($configuration->get(ConfigurationSchema::API_KEY)),
                new RequestAdapter(),
            )
        );
    }
}