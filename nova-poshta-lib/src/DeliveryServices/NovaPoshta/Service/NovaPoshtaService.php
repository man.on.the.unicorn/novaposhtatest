<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Service;

use Andry\DeliveryLibrary\Contracts\Service\Branch\Branch;
use Andry\DeliveryLibrary\Contracts\Service\Branch\BranchSearchParamBag;
use Andry\DeliveryLibrary\Contracts\Service\City\City;
use Andry\DeliveryLibrary\Contracts\Service\DeliveryService;
use Andry\DeliveryLibrary\Contracts\Service\DeliveryTypeId;
use Andry\DeliveryLibrary\Contracts\Shared\Pagination;
use Andry\DeliveryLibrary\Contracts\Shared\PaginationResult;
use Andry\DeliveryLibrary\DeliveryServices\DeliveryType;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Adapter\RequestAdapter;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\RequestFactory;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\NovaPoshtaBranch;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\NovaPoshtaCity;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\NovaPoshtaHttpClientInterface;

final class NovaPoshtaService implements DeliveryService
{
    public function __construct(
        private NovaPoshtaHttpClientInterface $client,
        private RequestFactory $requestFactory,
        private RequestAdapter $adapter,
    ) {
    }

    public function identifier(): DeliveryTypeId
    {
        return DeliveryType::NOVA_POSHTA;
    }

    public function getCity(string $cityId):? City
    {
        $novaPoshtaCity = $this->client->getCity($this->requestFactory->createCityRequest($cityId));

        return $novaPoshtaCity ? $this->adapter->adaptNovaPoshtaCity($novaPoshtaCity) : null;
    }

    /**
     * @inheritDoc
     */
    public function searchCity(Pagination $pagination): PaginationResult
    {
        $citiesResponse = $this->client->getCities($this->requestFactory->createCitiesRequest($pagination));

        return new PaginationResult(
            $pagination,
            (bool) $citiesResponse->info->totalCount,
            array_map(fn (NovaPoshtaCity $city) => $this->adapter->adaptNovaPoshtaCity($city), $citiesResponse->cities),
        );
    }

    /**
     * @inheritDoc
     */
    public function getBranch(string $branchId):? Branch
    {
        $novaPoshtaBranch = $this->client->getBranch($this->requestFactory->createBranchRequest($branchId));

        return $novaPoshtaBranch ? $this->adapter->adaptNovaPoshtaBranch($novaPoshtaBranch) : null;
    }

    /**
     * @inheritDoc
     */
    public function searchBranch(Pagination $pagination, string $cityId = null, BranchSearchParamBag $bag = null): PaginationResult
    {
        $branchesResponse = $this->client->getBranches($this->requestFactory->createBranchesRequest($pagination, $cityId));

        return new PaginationResult(
            $pagination,
            (bool) $branchesResponse->info->totalCount,
            array_map(fn (NovaPoshtaBranch $city) => $this->adapter->adaptNovaPoshtaBranch($city), $branchesResponse->branches),
        );
    }
}