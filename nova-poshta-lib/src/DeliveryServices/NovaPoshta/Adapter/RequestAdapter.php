<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Adapter;

use Andry\DeliveryLibrary\Contracts\Service\Branch\Branch;
use Andry\DeliveryLibrary\Contracts\Service\City\City;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\NovaPoshtaBranch;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\NovaPoshtaCity;

final class RequestAdapter
{
    public function adaptNovaPoshtaCity(NovaPoshtaCity $city): City
    {
        return new City($city->id, $city->name);
    }

    public function adaptNovaPoshtaBranch(NovaPoshtaBranch $branch): Branch
    {
        return new Branch($branch->id, $branch->cityId, $branch->name, $branch->latitude, $branch->longitude);
    }
}