<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request;

use JMS\Serializer\Annotation\SerializedName;

final class MethodProperties
{
    #[SerializedName(name: "Ref")]
    public readonly ?string $entityId;

    #[SerializedName(name: "CityRef")]
    public readonly ?string $cityId;

    #[SerializedName(name: "Page")]
    public readonly ?int $page;

    #[SerializedName(name: "Limit")]
    public readonly ?int $perPage;

    public function __construct(
        ?string $entityId = null,
        ?string $cityId = null,
        ?int $page = null,
        ?int $perPage = null,
    ) {
        $this->entityId = $entityId;
        $this->cityId = $cityId;
        $this->page = $page;
        $this->perPage = $perPage;
    }
}