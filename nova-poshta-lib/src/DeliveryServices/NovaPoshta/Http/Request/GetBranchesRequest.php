<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request;

use JMS\Serializer\Annotation\SerializedName;

final class GetBranchesRequest extends NovaPoshtaBaseRequest
{
    #[SerializedName(name: "methodProperties")]
    public readonly MethodProperties $methodProperties;

    public function __construct(
        string $apiKey,
        int $page,
        int $perPage,
        string $cityId = null,
    ) {
        parent::__construct($apiKey, 'AddressGeneral', 'getWarehouses');

        $this->methodProperties = new MethodProperties(
            cityId: $cityId,
            page: $page,
            perPage: $perPage,
        );
    }
}