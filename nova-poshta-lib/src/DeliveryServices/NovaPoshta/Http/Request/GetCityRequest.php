<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request;

use JMS\Serializer\Annotation\SerializedName;

final class GetCityRequest extends NovaPoshtaBaseRequest
{
    #[SerializedName(name: "methodProperties")]
    public readonly MethodProperties $methodProperties;

    public function __construct(
        string $apiKey,
        string $cityId,
    ) {
        parent::__construct($apiKey, 'Address', 'getCities');

        $this->methodProperties = new MethodProperties(entityId: $cityId);
    }
}
