<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request;

use JMS\Serializer\Annotation\SerializedName;

final class GetCitiesRequest extends NovaPoshtaBaseRequest
{
    #[SerializedName(name: "methodProperties")]
    public readonly MethodProperties $methodProperties;

    public function __construct(
        string $apiKey,
        int $page,
        int $perPage,
    ) {
        parent::__construct($apiKey, 'Address', 'getCities');

        $this->methodProperties = new MethodProperties(
            page: $page,
            perPage: $perPage,
        );
    }
}
