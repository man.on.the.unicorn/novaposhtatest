<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request;

use JMS\Serializer\Annotation as JMS;

abstract class NovaPoshtaBaseRequest
{
    #[JMS\SerializedName(name: "apiKey")]
    public readonly string $apiKey;

    #[JMS\SerializedName(name: "modelName")]
    public readonly string $modelName;

    #[JMS\SerializedName(name: "calledMethod")]
    public readonly string $calledMethod;

    public function __construct(
        string $apiKey,
        string $modelName,
        string $calledMethod,
    ) {
        $this->apiKey = $apiKey;
        $this->modelName = $modelName;
        $this->calledMethod = $calledMethod;
    }
}
