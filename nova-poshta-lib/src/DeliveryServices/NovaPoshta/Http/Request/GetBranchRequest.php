<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request;

use JMS\Serializer\Annotation\SerializedName;

final class GetBranchRequest extends NovaPoshtaBaseRequest
{
    #[SerializedName(name: "methodProperties")]
    public readonly MethodProperties $methodProperties;

    public function __construct(
        string $apiKey,
        string $branchId,
    ) {
        parent::__construct($apiKey, 'AddressGeneral', 'getWarehouses');

        $this->methodProperties = new MethodProperties(entityId: $branchId);
    }
}