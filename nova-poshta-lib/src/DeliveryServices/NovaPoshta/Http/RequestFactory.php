<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http;

use Andry\DeliveryLibrary\Contracts\Shared\Pagination;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request\GetBranchesRequest;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request\GetBranchRequest;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request\GetCitiesRequest;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request\GetCityRequest;

final class RequestFactory
{
    public function __construct(private string $apiKey)
    {
    }

    public function createCitiesRequest(Pagination $pagination): GetCitiesRequest
    {
        return new GetCitiesRequest($this->apiKey, $pagination->page, $pagination->perPage);
    }

    public function createCityRequest(string $cityId): GetCityRequest
    {
        return new GetCityRequest($this->apiKey, $cityId);
    }

    public function createBranchesRequest(Pagination $pagination, string $cityId = null): GetBranchesRequest
    {
        return new GetBranchesRequest($this->apiKey, $pagination->page, $pagination->perPage, $cityId);
    }

    public function createBranchRequest(string $branchId): GetBranchRequest
    {
        return new GetBranchRequest($this->apiKey, $branchId);
    }
}