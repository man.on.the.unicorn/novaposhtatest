<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http;

use Andry\DeliveryLibrary\Contracts\Validator\ValidatorInterface;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request\GetBranchesRequest;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request\GetBranchRequest;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request\GetCitiesRequest;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request\GetCityRequest;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request\NovaPoshtaBaseRequest;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\GetBranchesResponse;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\GetCitiesResponse;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\NovaPoshtaBaseResponse;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\NovaPoshtaBranch;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\NovaPoshtaCity;
use GuzzleHttp\ClientInterface;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Throwable;

final class NovaPoshtaHttpClient implements NovaPoshtaHttpClientInterface
{
    private const DEFAULT_FORMAT = 'json';
    private const API_VERSION = 'v2.0';

    public function __construct(
        private string $baseUrl,
        private ClientInterface $client,
        private ValidatorInterface $validator,
        private SerializerInterface $serializer,
        private LoggerInterface $logger,
    ) {
    }

    public function getCities(GetCitiesRequest $request): GetCitiesResponse
    {
        $this->validator->validate($request);

        $response = $this->postRequest($request, 'getCities', GetCitiesResponse::class);

        $this->validator->validate($response);

        return $response;
    }

    public function getCity(GetCityRequest $request):? NovaPoshtaCity
    {
        $this->validator->validate($request);

        $response = $this->postRequest($request, 'getCities', GetCitiesResponse::class);

        $this->validator->validate($response);

        return current($response->cities) ?: null;
    }

    public function getBranches(GetBranchesRequest $request): GetBranchesResponse
    {
        $this->validator->validate($request);

        $response = $this->postRequest($request, 'getWarehouses', GetBranchesResponse::class);

        $this->validator->validate($response);

        return $response;
    }

    public function getBranch(GetBranchRequest $request):? NovaPoshtaBranch
    {
        $this->validator->validate($request);

        $response = $this->postRequest($request, 'getWarehouses', GetBranchesResponse::class);

        $this->validator->validate($response);

        return current($response->branches) ?: null;
    }

    /**
     * @template T of NovaPoshtaBaseResponse
     * @psalm-param class-string<T> $responseClass
     *
     * @param NovaPoshtaBaseRequest $request
     * @param string $urlPart
     * @param string $responseClass
     * @return NovaPoshtaBaseResponse<T>
     * @throws NovaPoshtaResponseException
     */
    private function postRequest(
        NovaPoshtaBaseRequest $request,
        string $urlPart,
        string $responseClass,
    ): NovaPoshtaBaseResponse {

        try {
            $httpResponse = $this->client->request(
                'POST',
                $this->generateUrl($request, $urlPart),
                ['body' => $this->serializer->serialize($request, self::DEFAULT_FORMAT)]
            );

            /** @var NovaPoshtaBaseResponse $response */
            $response = $this->serializer->deserialize(
                $httpResponse->getBody()->__toString(),
                $responseClass,
                self::DEFAULT_FORMAT
            );

            if (count($response->errors)) {
                throw new NovaPoshtaResponseException(implode(';', $response->errors));
            }

            return $response;
        } catch (Throwable $throwable) {
            $this->logger->error("NovaPoshta 'postRequest' exception", [
                'message' => $throwable->getMessage(),
                'url' => $this->generateUrl($request, $urlPart),
            ]);

            throw new NovaPoshtaResponseException($throwable->getMessage(), 0, $throwable);
        }
    }

    private function generateUrl(NovaPoshtaBaseRequest $request, string $endUrlPart): string
    {
        return sprintf(
            '%s/%s/%s/%s/%s',
            rtrim($this->baseUrl, '/'),
            self::API_VERSION,
            self::DEFAULT_FORMAT,
            trim($request->modelName, '/'),
            $endUrlPart,
        );
    }
}