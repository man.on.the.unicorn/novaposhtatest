<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * @extends NovaPoshtaBaseResponse<GetCitiesResponse>
 */
final class GetCitiesResponse extends NovaPoshtaBaseResponse
{
    #[SerializedName(name: "data"), Type(name: "array<Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\NovaPoshtaCity>")]
    public readonly array $cities;
}