<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response;

use JMS\Serializer\Annotation\SerializedName;

final class ResponseInfo
{
    #[SerializedName(name: "totalCount")]
    public readonly int $totalCount;
}