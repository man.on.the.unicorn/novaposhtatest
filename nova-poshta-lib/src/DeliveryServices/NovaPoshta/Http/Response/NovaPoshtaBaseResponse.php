<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * @psalm-template T
 */
abstract class NovaPoshtaBaseResponse
{
    #[SerializedName(name: "success")]
    public readonly bool $success;

    #[SerializedName(name: "errors"), Type(name: "array<string>")]
    public readonly array $errors;

    #[SerializedName(name: "info"), Type(name: ResponseInfo::class)]
    public readonly ResponseInfo $info;
}