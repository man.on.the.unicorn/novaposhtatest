<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * @extends NovaPoshtaBaseResponse<GetBranchesResponse>
 */
final class GetBranchesResponse extends NovaPoshtaBaseResponse
{
    #[SerializedName(name: "data"), Type(name: "array<Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\NovaPoshtaBranch>")]
    public readonly array $branches;
}