<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response;

use JMS\Serializer\Annotation\SerializedName;

final class NovaPoshtaCity
{
    #[SerializedName(name: "Ref")]
    public readonly string $id;

    #[SerializedName(name: "DescriptionRu")]
    public readonly string $name;

    #[SerializedName(name: "IndexCOATSU1")]
    public readonly string $coastsuId;
}