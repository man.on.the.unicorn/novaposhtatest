<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response;

use JMS\Serializer\Annotation\SerializedName;

final class NovaPoshtaBranch
{
    #[SerializedName(name: "Ref")]
    public readonly string $id;

    #[SerializedName(name: "CityRef")]
    public readonly string $cityId;

    #[SerializedName(name: "DescriptionRu")]
    public readonly string $name;

    #[SerializedName(name: "Latitude")]
    public readonly ?string $latitude;

    #[SerializedName(name: "Longitude")]
    public readonly ?string $longitude;
}