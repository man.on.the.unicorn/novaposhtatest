<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http;

use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request\GetBranchesRequest;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request\GetBranchRequest;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request\GetCitiesRequest;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request\GetCityRequest;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\GetBranchesResponse;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\GetCitiesResponse;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\NovaPoshtaBranch;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Response\NovaPoshtaCity;

interface NovaPoshtaHttpClientInterface
{
    public function getCities(GetCitiesRequest $request): GetCitiesResponse;

    public function getCity(GetCityRequest $request):? NovaPoshtaCity;

    public function getBranches(GetBranchesRequest $request): GetBranchesResponse;

    public function getBranch(GetBranchRequest $request):? NovaPoshtaBranch;
}