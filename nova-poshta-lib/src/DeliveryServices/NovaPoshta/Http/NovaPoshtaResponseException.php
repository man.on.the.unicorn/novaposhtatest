<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http;

final class NovaPoshtaResponseException extends \Exception
{

}