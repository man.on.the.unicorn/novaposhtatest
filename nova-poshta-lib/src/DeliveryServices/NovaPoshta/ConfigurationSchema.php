<?php declare(strict_types=1);

namespace Andry\DeliveryLibrary\DeliveryServices\NovaPoshta;

final class ConfigurationSchema
{
    public const BASE_URL = 'base_url';
    public const API_KEY = 'api_key';
}