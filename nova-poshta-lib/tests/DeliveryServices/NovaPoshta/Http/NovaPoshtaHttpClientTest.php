<?php

namespace Andry\DeliveryLibrary\Tests\DeliveryServices\NovaPoshta\Http;

use Andry\DeliveryLibrary\Contracts\Validator\ValidatorInterface;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\NovaPoshtaHttpClient;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\NovaPoshtaResponseException;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Http\Request\GetCityRequest;
use GuzzleHttp\ClientInterface;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\NullLogger;

class NovaPoshtaHttpClientTest extends TestCase
{
    public function test_getCity_unexpectedResponse_shouldThrowException(): void
    {
        $this->expectException(NovaPoshtaResponseException::class);

        $httpPureResponse = '{"error": null}';

        $unexpectedResponse = $this->createMock(ResponseInterface::class);
        $unexpectedResponse
            ->expects(self::once())
            ->method('getBody')
            ->willReturn(new class implements \Stringable {
                public function __toString(): string
                {
                    return '{"error": null}';
                }
            });

        $guzzleClient = $this->createMock(ClientInterface::class);
        $guzzleClient
            ->method('request')
            ->willReturn($unexpectedResponse);

        $validator = $this->createMock(ValidatorInterface::class);
        $validator
            ->expects(self::once())
            ->method('validate');

        $serializer = $this->createMock(SerializerInterface::class);
        $serializer
            ->method('deserialize')
            ->with($httpPureResponse, 'getCities', 'json')
            ->willThrowException(new \Exception());

        $novaPoshtaHttpClient = new NovaPoshtaHttpClient(
            'https://dev.null',
            $guzzleClient,
            $validator,
            $serializer,
            new NullLogger(),
        );

        $novaPoshtaHttpClient->getCity(new GetCityRequest('123', '123'));
    }

    public function test_getCity_errorResponse_shouldThrowException(): void
    {
        $this->expectException(NovaPoshtaResponseException::class);

        $httpPureResponse = '{
  "success": false,
  "data": [
  ],
  "errors": ["this is some error"],
  "warnings": [],
  "info": {
    "totalCount": 0
  },
  "messageCodes": [],
  "errorCodes": [],
  "warningCodes": [],
  "infoCodes": []
}';

        $unexpectedResponse = $this->createMock(ResponseInterface::class);
        $unexpectedResponse
            ->expects(self::once())
            ->method('getBody')
            ->willReturn(new class ($httpPureResponse) implements \Stringable {
                public function __construct(private $httpPureResponse)
                {
                }

                public function __toString(): string
                {
                    return $this->httpPureResponse;
                }
            });

        $guzzleClient = $this->createMock(ClientInterface::class);
        $guzzleClient
            ->method('request')
            ->willReturn($unexpectedResponse);

        $validator = $this->createMock(ValidatorInterface::class);
        $validator
            ->expects(self::once())
            ->method('validate');

        $novaPoshtaHttpClient = new NovaPoshtaHttpClient(
            'https://dev.null',
            $guzzleClient,
            $validator,
            SerializerBuilder::create()->build(),
            new NullLogger(),
        );

        $novaPoshtaHttpClient->getCity(new GetCityRequest('123', '123'));
    }

    public function test_getCity_successResponse_shouldReturnCityObject(): void
    {
        $httpPureResponse = '{
  "success": true,
  "data": [
    {
      "Description": "Агрономічне",
      "DescriptionRu": "Агрономичное",
      "Ref": "ebc0eda9-93ec-11e3-b441-0050568002cf",
      "Delivery1": "1",
      "Delivery2": "1",
      "Delivery3": "1",
      "Delivery4": "1",
      "Delivery5": "1",
      "Delivery6": "0",
      "Delivery7": "0",
      "Area": "71508129-9b87-11de-822f-000c2965ae0e",
      "SettlementType": "563ced13-f210-11e3-8c4a-0050568002cf",
      "IsBranch": "0",
      "PreventEntryNewStreetsUser": null,
      "Conglomerates": null,
      "CityID": "890",
      "SettlementTypeDescriptionRu": "село",
      "SettlementTypeDescription": "село"
    }
  ],
  "errors": [],
  "warnings": [],
  "info": {
    "totalCount": 1
  },
  "messageCodes": [],
  "errorCodes": [],
  "warningCodes": [],
  "infoCodes": []
}';

        $unexpectedResponse = $this->createMock(ResponseInterface::class);
        $unexpectedResponse
            ->expects(self::once())
            ->method('getBody')
            ->willReturn(new class ($httpPureResponse) implements \Stringable {
                public function __construct(private $httpPureResponse)
                {
                }

                public function __toString(): string
                {
                    return $this->httpPureResponse;
                }
            });

        $guzzleClient = $this->createMock(ClientInterface::class);
        $guzzleClient
            ->method('request')
            ->willReturn($unexpectedResponse);

        $validator = $this->createMock(ValidatorInterface::class);
        $validator
            ->expects(self::exactly(2))
            ->method('validate');

        $novaPoshtaHttpClient = new NovaPoshtaHttpClient(
            'https://dev.null',
            $guzzleClient,
            $validator,
            SerializerBuilder::create()->build(),
            new NullLogger(),
        );

        $city = $novaPoshtaHttpClient->getCity(new GetCityRequest('123', '123'));

        $this->assertEquals($city->id, 'ebc0eda9-93ec-11e3-b441-0050568002cf');
    }
}
