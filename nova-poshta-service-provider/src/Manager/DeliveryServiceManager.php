<?php declare(strict_types=1);

namespace Andry\DeliveryServiceProvider\Manager;

use Andry\DeliveryLibrary\Contracts\Service\DeliveryService;
use Andry\DeliveryLibrary\DeliveryServices\DeliveryType;
use IteratorAggregate;

final class DeliveryServiceManager
{
    /** @var array<DeliveryService> */
    private readonly array $deliveryServices;

    public function __construct(IteratorAggregate $deliveryServices)
    {
        $this->deliveryServices = array_merge([], ...array_map(
            static fn (DeliveryService $service) => [$service->identifier()->identifierAsString() => $service],
            iterator_to_array($deliveryServices),
        ));
    }

    public function get(DeliveryType $identifier):? DeliveryService
    {
        return $this->deliveryServices[$identifier->identifierAsString()] ?? null;
    }
}