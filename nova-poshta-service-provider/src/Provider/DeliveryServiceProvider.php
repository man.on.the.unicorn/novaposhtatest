<?php declare(strict_types=1);

namespace Andry\DeliveryServiceProvider\Provider;

use Andry\DeliveryLibrary\Contracts\Configuration\ConfigurationBag;
use Andry\DeliveryLibrary\Contracts\Validator\NullValidator;
use Andry\DeliveryLibrary\Contracts\Validator\ValidatorInterface;
use Andry\DeliveryLibrary\DeliveryServices\DeliveryType;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\NovaPoshtaServiceProvider;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\Service\NovaPoshtaService;
use Andry\DeliveryServiceProvider\Manager\DeliveryServiceManager;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

final class DeliveryServiceProvider extends ServiceProvider
{
    private const TAG_NAME = 'delivery_service_provider.delivery_service';

    private array $targetServices = [
        NovaPoshtaService::class,
    ];

    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../Resources/delivery.php', 'delivery');

        $this->registerParticularDeliveryServices();

        $this->app->tag($this->targetServices, self::TAG_NAME);

        $this->app->singleton(DeliveryServiceManager::class, function () {
            return new DeliveryServiceManager($this->app->tagged(self::TAG_NAME));
        });
    }

    public function boot(): void
    {
        $this->publishes([
            __DIR__.'/../Resources/delivery.php' => config_path('delivery.php'),
        ]);
    }

    private function registerParticularDeliveryServices(): void
    {
        $this->app->singleton(NovaPoshtaService::class, function (Application $application) {
            $configuration = new ConfigurationBag($this->app->config['delivery'][DeliveryType::NOVA_POSHTA->value]);

            return (new NovaPoshtaServiceProvider(
                $application[SerializerInterface::class] ?? (new SerializerBuilder())->build(),
                $application[ValidatorInterface::class] ?? new NullValidator(),
                $application[ClientInterface::class] ?? new Client(),
                $application[LoggerInterface::class] ?? new NullLogger(),
            ))->provide($configuration);
        });
    }
}