<?php

namespace Andry\DeliveryServiceProvider\Manager;

use Andry\DeliveryLibrary\DeliveryServices\DeliveryType;
use Andry\DeliveryLibrary\DeliveryServices\NovaPoshta\ConfigurationSchema;

return [
    DeliveryType::NOVA_POSHTA->value => [
        ConfigurationSchema::BASE_URL => 'https://dev.null',
        ConfigurationSchema::API_KEY => 'api_key'
    ],
];
