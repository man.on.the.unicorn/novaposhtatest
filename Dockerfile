FROM php:8.1-fpm

ARG APP_ENV=production
ENV APP_ENV=${APP_ENV}

ENV APT_PHP_DEPS \
     freetds-bin \
     freetds-dev \
     freetds-common \
     libct4 \
     libsybdb5 \
     libfreetype6-dev \
     libzip-dev \
     libpq-dev \
     zlib1g-dev \
     libicu-dev \
     libxml2-dev \
     libssl-dev \
     libcurl4-openssl-dev \
     libmcrypt-dev

ENV PHP_STD_MOD \
    iconv \
    opcache \
    zip \
    exif \
    intl \
    pdo_mysql \
    mysqli

#PHP_MOD INSTALL
RUN apt-get update \
    && apt-get install -y $APT_PHP_DEPS --no-install-recommends \
    && docker-php-ext-install -j$(nproc) $PHP_STD_MOD

WORKDIR /var/www/application/

RUN useradd -d /var/www/application app && chown -hR app:app /var/www/application

COPY --chown=app:app ./nova-poshta-app /var/www/application
COPY --chown=app:app ./nova-poshta-lib /var/www/nova-poshta-lib
COPY --chown=app:app ./nova-poshta-service-provider /var/www/nova-poshta-service-provider
COPY ./docker /var/www/docker

RUN install /var/www/docker/fpm/${APP_ENV}.fpm.conf /usr/local/etc/php-fpm.d/docker.conf && install /var/www/docker/fpm/php-fpm.conf /usr/local/etc/php-fpm.conf && rm -f /usr/local/etc/php-fpm.d/www.conf && rm -f /usr/local/etc/php-fpm.d/zz-docker.conf
RUN install /var/www/docker/php/${APP_ENV}.99-overrides.ini /usr/local/etc/php/conf.d/99-overrides.ini

USER app

CMD ["php-fpm", "--nodaemonize"]